import { stringify } from '@angular/compiler/src/util';
import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  correo: string;
  nombre: string;
  apellido: string;
  empresa: string;
  direccion1: string;
  direccion2: string; 
  pais : string;
  ciudad: string;
  provincia: string;
  codigo_postal: string;
  telefono: string;
  codigo: string;
  valido: boolean;
  consentRemail: boolean;
  cupon: boolean = false;
  
  

  constructor(private router: Router) { }

  pasoParametro(){

    if(this.validar()){

      var persona : cliente = new cliente (this.correo, this.nombre , this.apellido , this.empresa , this.direccion1 , this.direccion2 , this.pais , this.ciudad , this.provincia , this.codigo_postal , this.telefono ,this.cupon, this.consentRemail );
    
      //console.table(miCoche);
   
       let navigationExtras: NavigationExtras = {
         state: {
           parametros: persona,
         }
       };
       this.router.navigate(['detalle'], navigationExtras);
    }
    
  }

  validarCupon(){

    if(this.codigo == undefined || this.codigo == ""){

      document.getElementById("codigo").style.borderColor="red";
      this.cupon = false;

    }else{
      document.getElementById("codigo").style.borderColor="black"; 
      this.cupon = true;
    }
  }

  validar(){
    this.valido = true;
    
    if(this.correo == undefined || this.correo == ""){

      document.getElementById("correo").style.borderColor="red";
      this.valido = false; 

    }else{
      document.getElementById("correo").style.borderColor="black"; 
    }
  
    
    if(this.nombre == undefined || this.nombre == ""){

      document.getElementById("nombre").style.borderColor="red";
      this.valido = false; 

    }else{
      document.getElementById("nombre").style.borderColor="black"; 
    }
    

    if(this.apellido == undefined || this.apellido == ""){

      document.getElementById("apellido").style.borderColor="red";
      this.valido = false; 

    }else{
      document.getElementById("apellido").style.borderColor="black"; 
    }
    //
    if(this.direccion1 == undefined || this.direccion1 == ""){

      document.getElementById("direccion1").style.borderColor="red"; 
      this.valido = false; 

    }else{
      document.getElementById("direccion1").style.borderColor="black"; 
    }
    //
    if(this.pais == undefined){

      document.getElementById("pais").style.borderColor="red";
      this.valido = false;  

    }else{
      document.getElementById("pais").style.borderColor="black"; 
    }
    //
    if(this.ciudad == undefined || this.ciudad == ""){

      document.getElementById("ciudad").style.borderColor="red";
      this.valido = false; 

    }else{
      document.getElementById("ciudad").style.borderColor="black"; 
    }
    //
    if(this.provincia == undefined){

      document.getElementById("provincia").style.borderColor="red";
      this.valido = false;  

    }else{
      document.getElementById("provincia").style.borderColor="black"; 
    }
    //
    if( this.codigo_postal == undefined || this.codigo_postal == ""){

      document.getElementById("codigo_postal").style.borderColor="red";
      this.valido = false; 
      
    }else if( isNaN(parseInt(this.codigo_postal)) || this.codigo_postal.length != 5){

      document.getElementById("codigo_postal").style.borderColor="red";
      this.valido = false; 

    }else document.getElementById("codigo_postal").style.borderColor="black"; 
    //

    if( this.telefono == undefined || this.telefono == ""){

      document.getElementById("telefono").style.borderColor="red";
      this.valido = false; 
      
    }else if( isNaN(parseInt(this.telefono)) || this.telefono.length != 9){

      document.getElementById("telefono").style.borderColor="red";
      this.valido = false; 

    }else document.getElementById("telefono").style.borderColor="black"; 
    
    return this.valido;

  }
}

class cliente{
  correo: string;
  nombre: string;
  apellido: string;
  empresa: string;
  direccion1: string;
  direccion2: string;
  pais : string;
  ciudad: string;
  provincia: string;
  codigo_postal: string;
  telefono: string;
  codigo: boolean;
  consentRemail: boolean;
  
  constructor(correo, nombre , apellido , empresa , direccion1 , direccion2 , pais , ciudad , provincia , codigo_postal , telefono ,codigo, consentRemail) {
    this.correo = correo; 
    this.nombre = nombre;
    this.apellido = apellido;
    this.empresa = empresa;
    this.direccion1 = direccion1;
    this.direccion2 = direccion2;
    this.pais = pais;
    this.ciudad = ciudad;
    this.provincia = provincia;
    this.codigo_postal = codigo_postal;
    this.telefono = telefono;
    this.codigo = codigo;
    this.consentRemail = consentRemail;
  }
}

 

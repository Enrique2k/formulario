import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  correo: string;
  nombre: string;
  apellido: string;
  empresa: string;
  direccion1: string;
  direccion2: string; 
  pais : string;
  ciudad: string;
  provincia: string;
  codigo_postal: string;
  telefono: string;
  codigo: boolean;
  valido: boolean;
 
  constructor(private route: ActivatedRoute, private router: Router) {

    this.route.queryParams.subscribe(params => {

      if (this.router.getCurrentNavigation().extras.state) {
        
        
        this.correo = this.router.getCurrentNavigation().extras.state.parametros["correo"];
        this.nombre = this.router.getCurrentNavigation().extras.state.parametros["nombre"];
        this.apellido = this.router.getCurrentNavigation().extras.state.parametros["apellido"];
        this.empresa = this.router.getCurrentNavigation().extras.state.parametros["empresa"];
        this.direccion1 = this.router.getCurrentNavigation().extras.state.parametros["direccion1"];
        this.direccion2 = this.router.getCurrentNavigation().extras.state.parametros["direccion2"];
        this.pais = this.router.getCurrentNavigation().extras.state.parametros["pais"];
        this.ciudad = this.router.getCurrentNavigation().extras.state.parametros["ciudad"];
        this.provincia = this.router.getCurrentNavigation().extras.state.parametros["provincia"];
        this.codigo_postal = this.router.getCurrentNavigation().extras.state.parametros["codigo_postal"];
        this.telefono = this.router.getCurrentNavigation().extras.state.parametros["telefono"];
        this.codigo = this.router.getCurrentNavigation().extras.state.parametros["codigo"];
        this.valido = this.router.getCurrentNavigation().extras.state.parametros["valido"];
      }
    });
    
  }


  ngOnInit() {
  }

}
